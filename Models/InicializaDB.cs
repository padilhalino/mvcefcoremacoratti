using System.Linq;

namespace MvcEFCore.Models
{
  public class InicializaDB
  {
    public static void Inicialize(ProdutoContexto context)
    {
      context.Database.EnsureCreated();

      // Procura por produtos
      if (context.Produtos.Any())
      {
        return; // O DB foi alimentado
      }

      var produtos = new Produto[]
      {
        new Produto() { Nome = "Caneta", Preco = 2.99M },
        new Produto() { Nome = "Borracha", Preco = 1.20M }
      };

      foreach (Produto p in produtos)
      {
        context.Produtos.Add(p);
      }
      context.SaveChanges();
    }
  }
}